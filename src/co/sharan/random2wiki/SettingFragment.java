package co.sharan.random2wiki;

import co.sharan.random2wiki.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

public class SettingFragment extends PreferenceFragment {
	Context ctx ;
	Activity acti;
	public Boolean checkL; 
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        checkL = false;
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        ctx = getActivity().getApplicationContext();
        acti = getActivity();
        
        
        Preference button = (Preference)findPreference("pref_cookie");
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference arg0) { 
                        	clearCookies();
                            //code for what you want it to do
                        	Toast.makeText(getActivity().getApplicationContext(), "Cookies cleared " ,
         		 				   Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    });
        
        Preference lsv = (Preference)findPreference("pref_language");
        lsv.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference arg0) { 
                        	 checkL = true;
                            return true;
                        }
                    });
       
    }
	public  void clearCookies(){
		
    	CookieSyncManager.createInstance(ctx);
    
    	CookieManager cookieManager = CookieManager.getInstance();
    	cookieManager.removeAllCookie();
    	
    }
	

}
