package co.sharan.random2wiki;

// license =  GNU General Public License (GPL)
//  problem ? title array out of bound exception for google site

import java.lang.reflect.Method;

import co.sharan.random2wiki.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	WebView wv;
	String currentUrl;
	String currentTitle;
	String randomUrl;
	String langDomain;
	Boolean stared = false;
	MenuItem star, refresh, share;
	DatabaseHandler db;
	String burl;


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    //inflater.inflate(R. menu);
	    inflater.inflate(R.menu.main_activity_actions,menu);
	    star = menu.findItem(R.id.action_star);
	    refresh = menu.findItem(R.id.action_refresh);
	    share = menu.findItem(R.id.menu_item_share);
	    
	    
	    rotate();
	    db = new DatabaseHandler(this);
	    return super.onCreateOptionsMenu(menu);
	    
	    
	   
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_refresh:
	            refresh();
	            return true;
	        case R.id.action_star:
	        	if(!stared){
	        		addBookmark();
	        	}
	        	else if(stared){
	        		removeBookmark();	        		
	        	}	        	
	        	return true;
	        case R.id.action_bookmarks:
	        	Intent i = new Intent(this, BookmarksActivity.class);
	        	startActivityForResult(i, 1);
	        	return true;
	      
	        case R.id.action_choose_language:
	        	Intent ii = new Intent(this, SettingsActivity.class);
	        	startActivity(ii);
	        	return true;
	        case R.id.menu_item_share:
	        	sharePage();
	        	return true;
	        	
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
      
        
        langDomain =  preferences.getString("pref_language", null);
        
        randomUrl = "http://" +langDomain+ ".wikipedia.org/w/index.php?title=Special:Random";
        
        Log.d("currenturl"," url "  + randomUrl);
        
        
      
        
        
        
        wv = (WebView) findViewById(R.id.webView1);
        
        
        
        
        wv.setWebViewClient(new WebViewClient(){
        	
        	public void onPageStarted(WebView view, String url, Bitmap favicon){
        		
					try {
						star.setEnabled(false);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
        	}
        	
			public void onPageFinished(WebView view, String url) {
				
				currentUrl = wv.getUrl();
				
				try {
					currentTitle = titleStrip(wv.getTitle());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					currentTitle = "";
				}
				
				
				checkBookmark();
				
				
				try {
					star.setEnabled(true);
					refresh.getActionView().clearAnimation();
					refresh.setActionView(null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.d("webview my", "some error has occured on page finshed");
					e.printStackTrace();
					e.getMessage();
					
				}
        		
        		
            }
			// handle webview clicks
			
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        		try {
					refresh.getActionView().clearAnimation();
					refresh.setActionView(null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                wv.loadUrl("file:///android_asset/myerrorpage.html");
            }
        	
        });
        wv.getSettings().setJavaScriptEnabled(true);    // enable javascript
        wv.getSettings().setBuiltInZoomControls(true);  //enable zoom
        wv.getSettings().setDisplayZoomControls(false);
        
       
        wv.loadUrl(randomUrl);
        
        
        
    }
    // handle bookmark ckic
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
             if(resultCode == RESULT_OK){
              burl=data.getStringExtra("burl");
              wv.loadUrl(burl);
             }
        }
    } 
    
	public void refresh(){ 
		rotate();
    	wv.loadUrl(randomUrl); 	
    	
    	
    }
    public void rotate(){
    	
        /* Attach a rotating ImageView to the refresh item as an ActionView */
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.action_progressbar, null);

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);
      
        refresh.setActionView(iv);
    }
    
    
    // code for back button handling === works only when user clicks any link in page
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(wv.canGoBack()){
                    wv.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    
    // wikipedia strip extra tilte
    public String titleStrip(String t)
    {
    	if(t.isEmpty())
    		return "";
    	int i = t.indexOf(" - Wikipedia");
    	if(i!=-1)
    		return t.substring(0, i);
    	return t;
    }
    
    public void checkBookmark(){
    	Boolean b = db.checkBookmark(currentUrl);
    	Log.d("bookmark", Boolean.toString(b));
    	if(b){
    		stared=true;
    		star.setIcon(R.drawable.stared);
    	}else{
    		stared=false;
    		star.setIcon(R.drawable.ic_action_not_important);
    	}
    	
    }
    public void addBookmark(){
		if (!currentUrl.endsWith("#/search")) {
			db.addBookmark(new Bookmark(currentTitle, currentUrl));
			stared = true;
			star.setIcon(R.drawable.stared);
			Toast.makeText(getApplicationContext(),
					"Page Bookmarked!", Toast.LENGTH_SHORT).show();
		}
    }
    public void removeBookmark(){
    	
    	new AlertDialog.Builder(this)
	    .setTitle("Remove")
	    .setMessage(currentTitle)
	    .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        	db.deleteBookmarkByUrl(currentUrl);
	        	Toast.makeText(getApplicationContext(), "Bookmark removed!",
		 				   Toast.LENGTH_SHORT).show();
	        	stared=false;
        		star.setIcon(R.drawable.ic_action_not_important);
	        	
	        }
	     })
	    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	           
	        }
	     })
	    .setIcon(R.drawable.ic_action_warning)
	    .show();
    }
    public  void clearCookies(){
    	CookieSyncManager.createInstance(this);
    	CookieManager cookieManager = CookieManager.getInstance();
    	cookieManager.removeAllCookie();
    	Toast.makeText(getApplicationContext(),
				"cookies cleared", Toast.LENGTH_SHORT).show();
    }
    private void sharePage() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
     
        share.putExtra(Intent.EXTRA_SUBJECT, currentTitle);
        share.putExtra(Intent.EXTRA_TEXT, currentUrl);
     
        startActivity(Intent.createChooser(share, "Share page!"));
    }
    
  

}
