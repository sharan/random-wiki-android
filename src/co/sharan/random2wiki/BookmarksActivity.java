package co.sharan.random2wiki;

import java.util.List;

import co.sharan.random2wiki.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BookmarksActivity extends Activity {
	
	
	ListView bList;
	DatabaseHandler db;
	int pos;
	ArrayAdapter<Bookmark> adapter;
	List<Bookmark> bookmarks;
	Bookmark cb; //current bookmark
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmarks);
		db = new DatabaseHandler(this);

		
		/* back button */
		getActionBar().setDisplayHomeAsUpEnabled(true);

		
		populateBookmarks();
		
		//handle click
		bList.setOnItemClickListener(new OnItemClickListener() {
		  @Override
		  public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
			  
	       
	            Intent intent = new Intent();
	            intent.putExtra("burl",adapter.getItem(position).getUrl());
	            setResult(RESULT_OK, intent);        
	            finish();
		  }
		});
		
		
	    
	   
	}
	
	public void populateBookmarks(){
		bookmarks = db.getAllBookmarks();      
        
		adapter = new ArrayAdapter<Bookmark>(this,R.layout.bookmarks_item,R.id.textView1,bookmarks);
		bList = (ListView) findViewById(R.id.bookmarksList);
		bList.setAdapter(adapter);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button without restarting activity
	    case android.R.id.home:
	        finish();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	public void deleteBookmark(View v){
		
		
		pos = bList.getPositionForView(v);
		cb = bookmarks.get(pos);
		
		new AlertDialog.Builder(this)
		    .setTitle("Remove")
		    .setMessage(cb.getTitle())
		    .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // continue with delete
		        	db.deleteBookmark(cb);
		        	adapter.remove(cb);
		        	//bookmarks= db.getAllBookmarks();
		        	
		        	adapter.notifyDataSetChanged();
		        	Toast.makeText(getApplicationContext(), "Bookmark Removed!",
		 				   Toast.LENGTH_SHORT).show();
		        }
		     })
		    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        	
		        }
		     })
		    .setIcon(R.drawable.ic_action_warning)
     .show();


	}
}


