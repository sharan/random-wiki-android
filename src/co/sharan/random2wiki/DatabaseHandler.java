package co.sharan.random2wiki;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "Bookmarks";
 
    // Contacts table name
    private static final String TABLE_BOOKMARKS = "bookmarks";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_URL = "url";
 
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_BOOKMARKS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_URL + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKMARKS);
 
        // Create tables again
        onCreate(db);
    }
    
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
 
    // Adding new contact
    void addBookmark(Bookmark bookmark) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, bookmark.getTitle()); // Contact Name
        values.put(KEY_URL, bookmark.getUrl()); // Contact Phone
 
        // Inserting Row
        db.insert(TABLE_BOOKMARKS, null, values);
        db.close(); // Closing database connection
    }
 
    // Getting single contact
    Bookmark getBookmark(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_BOOKMARKS, new String[] { KEY_ID,
                KEY_TITLE, KEY_URL }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        Bookmark bookmark = new Bookmark(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return bookmark;
    }
    // Getting single contact
    Boolean checkBookmark(String passedUrl) {
    	if(passedUrl.endsWith("#/search")){
    		return false;
    	}
    	Log.d("bookmark", "bookmark string is "+ passedUrl);
        SQLiteDatabase db = this.getReadableDatabase();
        
        String query = "SELECT * FROM " + TABLE_BOOKMARKS + " WHERE  url=\"" + passedUrl +"\"";
        Log.d("bookmark query", " bookmark " + query);
        Cursor mCursor = db.rawQuery(query,null);
        
        if (mCursor.moveToFirst()) {
        	// record exists
        	Log.d("bookmark", " bookmark in check exist");
            return true;
        	} else {
        	// record not found
        		Log.d("bookmark", " bookmark does not exsit");
                return false;
        	}
      
    }
    //delete by url
    void deleteBookmarkByUrl(String passedUrl) {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(TABLE_BOOKMARKS, KEY_URL + " = ?",
                new String[] { passedUrl });
      
    }
     
    // Getting All Contacts
    public List<Bookmark> getAllBookmarks() {
        List<Bookmark> bookmarkList = new ArrayList<Bookmark>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKMARKS;
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bookmark bookmark = new Bookmark();
                bookmark.setid(Integer.parseInt(cursor.getString(0)));
                bookmark.setTitle(cursor.getString(1));
                bookmark.setUrl(cursor.getString(2));
                // Adding contact to list
                bookmarkList.add(bookmark);
            } while (cursor.moveToNext());
        }
 
        // return contact list
        return bookmarkList;
    }
 
    // Updating single contact
    public int updateBookmark(Bookmark bookmark) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, bookmark.getTitle());
        values.put(KEY_URL, bookmark.getUrl());
 
        // updating row
        return db.update(TABLE_BOOKMARKS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(bookmark.getid()) });
    }
 
    // Deleting single contact
    public void deleteBookmark(Bookmark bookmark) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOOKMARKS, KEY_ID + " = ?",
                new String[] { String.valueOf(bookmark.getid()) });
        db.close();
    }
 
 
    // Getting contacts Count
    public int getBookmarksCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BOOKMARKS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
 
        // return count
        return cursor.getCount();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}