package co.sharan.random2wiki;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;


public class SettingsActivity extends Activity {
	SettingFragment sf;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SettingFragment();
       
        
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, sf)
                .commit();
        

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

	// for not restrating mainactivity
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home:
	    	if(sf.checkL){
	    		startActivity(new Intent(this, MainActivity.class));
	    	}else{
	    		finish();
	    	}
	    		
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		if(sf.checkL){
    		startActivity(new Intent(this, MainActivity.class));
    	}else{
    		finish();
    	}
	}
	

}