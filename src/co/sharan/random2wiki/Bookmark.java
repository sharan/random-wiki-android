package co.sharan.random2wiki;
//bookmark model
public class Bookmark {
	int id;
    String title;
    String url;

    public Bookmark(String title, String url) {

        this.title = title;
        this.url = url;
    }
    public Bookmark(int _id,String title, String url) {
    	this.id = _id;
        this.title = title;
        this.url = url;
    }
    
    public Bookmark(){}
    
    public int getid(){
    	return id;
    }
    public void setid(int _id){
    	this.id= _id;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    @Override
    public String toString() {
     return getTitle();
    }

}
